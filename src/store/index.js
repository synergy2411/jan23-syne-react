import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from '@redux-devtools/extension'

import logger from './middlewares/logger';
import rootReducer from './reducers/rootReducer';

const store = createStore(combineReducers(rootReducer),
    composeWithDevTools(applyMiddleware(logger, thunk)));

export default store;