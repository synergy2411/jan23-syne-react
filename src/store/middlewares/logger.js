// const logger = (store) => (next) => (action) => {}

function logger(store) {
    return function (next) {
        return function (action) {
            // perform any side effect operation
            console.log("[LOGGER STATE]", store.getState());
            console.log("[LOGGER ACTION]", action);
            return next(action)


            // setTimeout(() => {
            //     return next(action)
            // if (action.type === "INCREMENT") {
            //     return next({ type: "DECREMENT" })         // allow to pass the action to the reducer
            // } else {
            //     return next(action)
            // }
            // }, 1000)
        }
    }
}

export default logger;