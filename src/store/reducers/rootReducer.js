import counterReducer from "./counter.reducer";
import resultReducer from "./result.reducer";
import todoReducer from "./todo.reducer";

const rootReducer = {
    ctr: counterReducer,
    res: resultReducer,
    todo: todoReducer
}

export default rootReducer;