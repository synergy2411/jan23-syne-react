import * as actionTypes from '../actions/actionTypes';

const initialState = {
    result: []
}

function resultReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.STORE_RESULT: {
            return {
                ...state,
                result: [action.payload, ...state.result]
            }
        }
        case actionTypes.DELETE_RESULT: {
            const filteredResult = state.result.filter((val, index) => index !== action.payload)
            return {
                ...state,
                result: filteredResult
            }
        }
        default:
            return state;
    }
}

export default resultReducer;