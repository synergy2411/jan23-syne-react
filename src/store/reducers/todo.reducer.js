import * as actionTypes from '../actions/actionTypes';

const initialState = {
    todos: [],
    loading: null,
    error: null
}

function todoReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.ADD_NEW_TODO_STARTED: {
            return {
                ...state,
                loading: true
            }
        }

        case actionTypes.ADD_NEW_TODO_SUCCESS: {
            return {
                ...state,
                loading: false,
                error: null,
                todos: [action.payload, ...state.todos]
            }
        }
        case actionTypes.ADD_NEW_TODO_FAILURE: {
            return {
                ...state,
                loading: false,
                error: action.paload
            }
        }

        case actionTypes.FETCH_TODOS_STARTED: {
            return {
                ...state,
                loading: true,
                error: null
            }
        }
        case actionTypes.FETCH_TODOS_SUCCESS: {
            console.log("FETCH_TODOS_SUCCESS", action);
            return {
                ...state,
                todos: [...action.payload, ...state.todos],
                loading: false,
                error: null
            }
        }
        case actionTypes.FETCH_TODOS_FAILURE: {
            console.log("FETCH_TODOS_FAILURE", action);
            return {
                error: action.payload,
                loading: false
            }
        }
        default:
            return state;
    }
}

export default todoReducer;