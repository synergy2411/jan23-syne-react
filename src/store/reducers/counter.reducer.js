import * as actionTypes from '../actions/actionTypes';

const initialState = {
    counter: 0
}

function counterReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.INCREMENT: {
            // state.counter++             // NEVER EVER CHANGE/MUTATE THE STATE
            return {
                ...state,
                counter: state.counter + 1
            }
        }
        case actionTypes.DECREMENT: {
            return {
                ...state,
                counter: state.counter - 1
            }
        }
        case actionTypes.ADD_COUNTER: {
            return {
                ...state,
                counter: state.counter + action.payload
            }
        }
        case actionTypes.SUBTRACT_COUNTER: {
            return {
                ...state,
                counter: state.counter - action.payload
            }
        }
        default:
            return state;
    }
}

export default counterReducer;