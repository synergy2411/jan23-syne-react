export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const ADD_COUNTER = "ADD_COUNTER";
export const SUBTRACT_COUNTER = "SUBTRACT_COUNTER";
export const STORE_RESULT = "STORE_RESULT";
export const DELETE_RESULT = "DELETE_RESULT";
export const FETCH_TODOS = "FETCH_TODOS"
export const FETCH_TODOS_STARTED = "FETCH_TODOS_STARTED"
export const FETCH_TODOS_SUCCESS = "FETCH_TODOS_SUCCESS"
export const FETCH_TODOS_FAILURE = "FETCH_TODOS_FAILURE"
export const ADD_NEW_TODO_STARTED = "ADD_NEW_TODO"
export const ADD_NEW_TODO_SUCCESS = "ADD_NEW_TODO_SUCCESS"
export const ADD_NEW_TODO_FAILURE = "ADD_NEW_TODO_FAILURE"

// Async Action Creators

export function onAddTodo(label) {
    return function (dispatch) {
        dispatch({ type: ADD_NEW_TODO_STARTED })
        fetch("http://localhost:9000/todos", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ label: label, status: "pending" })
        }).then(resp => resp.json())
            .then(data => dispatch(addTodoSuccess(data)))
            .catch(err => dispatch(addTodoFailure(err.message)))
    }
}

export function fetchTodos() {
    return function (dispatch) {
        dispatch({ type: FETCH_TODOS_STARTED })

        fetch("http://localhost:9000/todos")
            .then(resp => resp.json())
            .then(data => {
                dispatch(onFetchTodoSuccess(data))
            }).catch(err => {
                dispatch(onFetchTodoFailure(err.message))
            })
    }
}

export function asyncIncrement() {
    return function (dispatch) {
        // Run any side effect code here
        setTimeout(() => {
            dispatch(onIncrement())
        }, 2000)
    }
}

// Action Creators

export function addTodoSuccess(data) {
    return { type: ADD_NEW_TODO_SUCCESS, payload: data }
}
export function addTodoFailure(errorMessage) {
    return { type: ADD_NEW_TODO_FAILURE, payload: errorMessage }
}

export function onFetchTodoSuccess(data) {
    return {
        type: FETCH_TODOS_SUCCESS,
        payload: data
    }
}

export function onFetchTodoFailure(errorMessage) {
    return {
        type: FETCH_TODOS_FAILURE,
        payload: errorMessage
    }
}

export function onIncrement() {
    return {
        type: INCREMENT
    }
}

export function onDeleteResult(idx) {
    return {
        type: DELETE_RESULT,
        payload: idx
    }
}

export function onStoreResult(payload) {
    return {
        type: STORE_RESULT,
        payload
    }
}

export function onSubtractCounter(value) {
    // to perform any operation on the value
    return {
        type: SUBTRACT_COUNTER,
        payload: value
    }
}
