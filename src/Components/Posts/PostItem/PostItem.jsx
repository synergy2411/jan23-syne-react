import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { BounceLoader } from 'react-spinners'
import classes from './PostItem.module.css';

const PostItem = () => {

    const params = useParams();
    const navigate = useNavigate()
    const [post, setPost] = useState(null);

    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${params.postId}`)
            .then(resp => resp.json())
            .then(data => {
                setPost(data)
            }).catch(console.error)
    }, [params])

    const closeClickHandler = () => { navigate("/posts") }

    const deleteClickHandler = () => {
        fetch(`http://localhost:9000/posts/${post.id}`, {
            method: "DELETE"
        }).then(resp => resp.json())
            .then(data => {
                console.log("DELETED " + data.toString())
                navigate("/posts")
            }).catch(console.error)
    }

    if (post !== null) {
        return (
            <div className={classes['backdrop']}>
                <div className={classes['my-dialog']}>
                    <div className='card'>
                        <div className='card-body'>
                            <h6 className='text-center'>{post.id} - {post.title}</h6>
                            <p className='text-center'>{post.body}</p>
                            <div className='d-grid gap-4'>
                                <button className='btn btn-dark btn-sm' onClick={deleteClickHandler}>DELETE</button>
                                <button className='btn btn-warning btn-sm' onClick={closeClickHandler}>CLOSE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return <BounceLoader color="#ff45ff" size={120} />
}

export default PostItem;

