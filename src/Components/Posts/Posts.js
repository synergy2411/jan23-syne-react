import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';

import classes from './Posts.module.css';

const sortPosts = (isAscending, posts) => {
    if (isAscending) {
        return posts.sort((a, b) => {
            if (a.title > b.title) {
                return 1;
            } else if (b.title > a.title) {
                return -1
            } else {
                return 0
            }
        })
    } else {
        return posts.sort((a, b) => {
            if (a.title > b.title) {
                return -1;
            } else if (b.title > a.title) {
                return 1
            } else {
                return 0
            }
        })
    }
}

const Posts = () => {

    const navigate = useNavigate();
    const location = useLocation();
    const query = new URLSearchParams(location.search);
    const isAscending = query.get("order") === 'asc'

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            fetch("http://localhost:9000/posts")
                .then(resp => resp.json())
                .then(data => {
                    const posts = data.filter((post, index) => index < 5)
                    setPosts(posts);
                }).catch(console.error)
        }, 1500)

    }, [])

    const itemClickHandler = (postId) => {
        navigate(`/posts/${postId}`)                    // http://localhost:3000/posts/2
    }

    const orderClickHandler = () => navigate(`/posts?order=${isAscending ? 'desc' : 'asc'}`)

    const sortedPosts = sortPosts(isAscending, posts);

    if (posts.length > 0) {
        return (
            <React.Fragment>
                <div className='row'>
                    <div className='col-4 offset-4'>
                        <div className='d-grid'>
                            <button className='btn btn-secondary'
                                onClick={orderClickHandler}>
                                {isAscending ? 'Decending' : 'Ascending'}
                            </button>
                        </div>
                    </div>
                </div>
                <ul className='list-group'>
                    {sortedPosts.map((post) => (
                        <li onClick={() => itemClickHandler(post.id)} className={`list-group-item mb-3 ${classes['my-list']}`} key={post.id}>
                            <p>{post.title.toUpperCase()}</p>
                        </li>
                    ))}

                </ul>

                {/* Nested Routing / Child Routing */}
                {/* <Routes>
                    <Route path="/:postId" element={<PostItem />} />
                </Routes> */}

            </React.Fragment>
        );
    }
    return <BounceLoader color="#ff45ff" size={120} />
}

export default Posts;

                // <div className='row'>
                //     <div className='col-4'>
                //         <div className='card'>
                //             <div className='card-body'>
                //                 <p>{posts[0].title.toUpperCase()}</p>
                //             </div>
                //         </div>
                //     </div>
                // </div>