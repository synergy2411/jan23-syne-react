import React from 'react';

const Child = (props) => {
    console.log("[CHILD]");
    return (
        <div>
            <h5>The Child Component</h5>
            <button onClick={() => props.dummyFn()}>Call Dummy</button>
            {props.toggle && <p>This paragraph will be toggled</p>}
        </div>
    );
}

export default React.memo(Child);

// props.oldValue === props.newValue
