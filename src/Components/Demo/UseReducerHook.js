import React, { useReducer } from 'react';

function reducerFn(state, action) {
    if (action.type === "INCREMENT") {
        return {
            ...state,
            counter: state.counter + 1
        }
    }
    if (action.type === "DECREMENT") {
        return {
            ...state,
            counter: state.counter - 1
        }
    }
    if (action.type === "STORE_RESULT") {
        return {
            ...state,
            result: [state.counter, ...state.result]
        }
    }
    return state;
}

const UseReducerHook = () => {
    const [state, dispatch] = useReducer(reducerFn, { counter: 0, result: [] })

    return (
        <div>
            <p className='display-4'>Counter : {state.counter}</p>
            <button className='btn btn-primary p-3 btn-lg'
                onClick={() => dispatch({ type: "INCREMENT" })}>+</button>
            <button className='btn btn-dark p-3 btn-lg'
                onClick={() => dispatch({ type: "DECREMENT" })}>-</button>
            <br />
            <button className='btn btn-warning'
                onClick={() => dispatch({ type: "STORE_RESULT" })}>Store Result</button>
            <hr />
            <ul>
                {state.result.map(r => <li key={r}>{r}</li>)}
            </ul>

        </div>
    );
}

export default UseReducerHook;
