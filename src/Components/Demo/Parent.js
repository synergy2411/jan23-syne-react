import React, { useCallback, useMemo, useState } from 'react';
import Child from './Child';


const Parent = () => {
    const [toggle, setToggle] = useState(true)

    const dummyFn = useCallback(() => console.log("The Dummy function"), []);
    const memoFn = useMemo(() => () => console.log("Memoized Function"), [])

    const friends = useMemo(() => ["Monica", "Joe", "Chandler"], []);


    console.log("[PARENT]");
    return (
        <div>
            <h4>The Parent Component</h4>
            <button className='btn btn-primary'
                onClick={() => setToggle(!toggle)}>Toggle</button>
            <Child toggle={true} dummyFn={dummyFn} friends={friends} />
        </div>
    );
}

export default Parent;
