import { Component } from 'react';

class ClassBased extends Component {

    notifer = null;
    constructor(props) {
        super(props);
        console.log("[CONSTRUCTOR]")
        this.state = {
            counter: 0,
            posts: []
        }
    }

    componentDidMount() {
        console.log("[COMPONENT DID MOUNT]")
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(resp => resp.json())
            .then(data => {
                this.setState({
                    posts: data
                })
            }).catch(console.error)

    }

    componentDidUpdate() {
        //  this.notifer = setInterval(() => {console.log("Timer")}, 1000)
        console.log("[COMPONENT DID UPDATE]");
    }

    componentWillUnmount() {
        // release the memory
        // eg. Clear Time | unsubscribe the Subscriptions | GRaphql API unsubscription
        // clearInterval(this.notifer);
        // this.var = null;
        console.log("[COMPONENT WILL UNMOUNT]");
    }

    shouldComponentUpdate() {
        console.log("[SHOULD COMPONENT UPDATE]");
        return true;
    }

    increaseHandler = () => {
        // this.state.counter++         // NEVER EVER CHANGE THE STATE
        this.setState({ counter: this.state.counter + 1 })
    }
    render() {
        console.log("[RENDER]")
        return (
            <div>
                <p>Counter : {this.state.counter} </p>
                <button className='btn btn-primary'
                    onClick={this.increaseHandler} >
                    Increase</button>
                <hr />
                {this.state.posts.length > 0 && <p>Title : {this.state.posts[0].title}</p>}
            </div>
        )
    }
}

export default ClassBased;