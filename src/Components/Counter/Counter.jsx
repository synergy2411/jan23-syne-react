import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/actionTypes";
import Result from "./Result/Result";

class Counter extends Component {
  render() {
    return (
      <>
        <p className="text-center display-5">Counter : {this.props.counter}</p>
        <br />
        <div className="text-center">
          <button className="btn btn-primary" onClick={this.props.onIncrement}>
            Increase
          </button>
          <button className="btn btn-dark" onClick={this.props.onDecrement}>
            Decrease
          </button>
          <button
            className="btn btn-success"
            onClick={() => this.props.onAddCounter(10)}
          >
            Add Counter
          </button>
          <button
            className="btn btn-warning"
            onClick={() => this.props.onSubstractCtr(5)}
          >
            Subtract Counter
          </button>
        </div>
        <hr />
        <Result />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    counter: state.ctr.counter,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onIncrement: () => dispatch(actionTypes.asyncIncrement()),
    onDecrement: () => dispatch({ type: actionTypes.DECREMENT }),
    onAddCounter: (value) =>
      dispatch({ type: actionTypes.ADD_COUNTER, payload: value }),
    onSubstractCtr: (value) => dispatch(actionTypes.onSubtractCounter(value)),
  };
};

const ConnectCounter = connect(mapStateToProps, mapDispatchToProps);

export default ConnectCounter(Counter);

// export default connect(mapStateToProps, mapDispatchToProps) (Counter);
