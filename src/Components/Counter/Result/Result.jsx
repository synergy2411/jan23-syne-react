import React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actionTypes from "../../../store/actions/actionTypes";

const Result = () => {
  const result = useSelector((store) => store.res.result);
  const counter = useSelector((store) => store.ctr.counter);
  const dispatch = useDispatch();

  return (
    <>
      <div className="row">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button
              className="btn btn-outline-danger"
              onClick={() => dispatch(actionTypes.onStoreResult(counter))}
            >
              STORE RESULT
            </button>
          </div>
        </div>
      </div>
      {result.length > 0 && (
        <div className="row">
          <div className="col-6 offset-3">
            <ul className="list-group">
              {result.map((r, i) => (
                <li
                  className="list-group-item text-center mb-2"
                  key={i}
                  onClick={() => dispatch(actionTypes.onDeleteResult(i))}
                >
                  {r}
                </li>
              ))}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};

export default Result;
