import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import * as actionTypes from "../../../store/actions/actionTypes";

const AddTodo = () => {
  const dispatch = useDispatch();
  const [enteredTodoLabel, setEnteredTodoLabel] = useState("");
  return (
    <>
      <form>
        <div className="form-label">
          <div className="row">
            <div className="col-8">
              <input
                type="text"
                className="form-control"
                value={enteredTodoLabel}
                onChange={(event) => setEnteredTodoLabel(event.target.value)}
              />
            </div>
            <div className="col-4">
              <div className="d-grid">
                <button
                  className="btn btn-dark"
                  onClick={() =>
                    dispatch(actionTypes.onAddTodo(enteredTodoLabel))
                  }
                >
                  Add
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddTodo;
