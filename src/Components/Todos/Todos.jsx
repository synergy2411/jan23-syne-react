import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { BounceLoader } from "react-spinners";

import * as actionTypes from "../../store/actions/actionTypes";
import AddTodo from "./AddTodo/AddTodo";

const Todos = () => {
  const { loading, todos, error } = useSelector((state) => state.todo);
  const dispatch = useDispatch();

  if (loading) {
    return <BounceLoader size={120} color="#ff45ff" />;
  }

  if (error) {
    return (
      <div className="alert alert-danger">
        <h2>Something went wrong</h2>
        <p>{error} </p>
      </div>
    );
  }
  return (
    <div>
      <h1>All Todos</h1>
      <AddTodo />
      <hr />
      <button
        className="btn btn-success"
        onClick={() => dispatch(actionTypes.fetchTodos())}
      >
        Fetch Todos
      </button>
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>{todo.label}</li>
        ))}
      </ul>
    </div>
  );
};

export default Todos;
