import ExpenseDate from "../ExpenseDate/ExpenseDate";

const ExpenseItem = (props) => {

    return (
        <div className="col-4">
            <div className="card">
                <div className="card-header">
                    <h6 className="text-center"> {props.expense.title.toUpperCase()} </h6>
                </div>
                <div className="card-body">
                    <p>Amount : ${props.expense.amount}</p>
                    <ExpenseDate createdAt={props.expense.createdAt} />
                    <br />
                    <div className="d-flex justify-content-end">
                        <button className="btn btn-outline-danger btn-sm"
                            onClick={() => props.onDeleteExpense(props.expense.id)}>
                            DELETE
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ExpenseItem;