import React from 'react';

const ExpenseFilter = (props) => {
    return (
        <select className='form-control'
            value={props.selectedYear}
            onChange={(event) => props.onYearSelected(event.target.value)}>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
        </select>
    );
}

export default ExpenseFilter;
