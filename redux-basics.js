// // import redux from 'redux';           // ESM Import - Browsers
// const redux = require("redux");         // CommonJS Import - Node Platform


// const initialState = {
//     counter: 0
// }

// // Creating 'Reducer'
// function rootReducer(prevState = initialState, action) {
//     if (action.type === "INCREMENT") {
//         return {
//             counter: prevState.counter + 1
//         }
//     }
//     if (action.type === "ADD_COUNTER") {
//         return {
//             counter: prevState.counter + action.payload
//         }
//     }
//     return prevState;
// }

// // Creating 'Store'
// const store = redux.createStore(rootReducer);

// // Subscribe
// store.subscribe(() => {
//     console.log("[CURRENT STATE]", store.getState())
// })

// // console.log("[STATE]", store.getState())

// // Dispatch Action
// store.dispatch({ type: "INCREMENT" })
// // console.log("[AFTER INCREMENT ACTION : STATE]", store.getState())

// store.dispatch({ type: "ADD_COUNTER", payload: 10 })

let friends = ["foo", "bam", "bar"]

let filteredFriends = friends.filter((value, idx, array) => {
    console.log(value)
    console.log(idx)
    console.log(array)
    return idx !== 1
})


console.log("Filtered Friends : ", filteredFriends);
