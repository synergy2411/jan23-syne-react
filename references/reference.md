# Break Timing

- Tea Break -> 11:00AM (15 mins)
- Lunch Break -> 01:00 (45 mins)
- Tea Break -> 03:30 (15 mins)

# What is React (30-35kb)?

- ReactDOM : bridge lib for DOM and React
- Uses Virtual DOM
- Can create SPA (react-router-dom)
- Can write HTML & JavaScript
- JavaScript Library
- Can create reusable components
- Avoid unnecessarily page re-rendering
- optimized code (Webpack)
- Fast UI
- Component based approach
- State Management (Redux)
- Form Validation (formik, react-form-hook etc)
- Corporate Caretaker (Facebook Team)
- Community Support

# Other JavaScript Libraries and Frameworks -

- React (View Part of MVC) : to render the UI efficiently
- \*Angular : Google Sponsured, SPA, Framework, Material UI, ngbootstrap, DI, Routing, Lazy loading, Testing
- \*Ember : very frequent changes in internal APIs
- \*NextJS : Framework for React (SSR)
- jQuery : DOM manipulation; AJAX Call
- Vue : Light-weight, Template oriented, "Evan You"
- Vuex : State Management
- Svelte :
- Redux : State Management
- Backbone : MVC Pattern at client side
- Knockout : MVVM Patten at client; 2 way data binding

- NodeJS : Platform to run JavaScript Code
- ExpressJS/ Koa / Hapi / Kraken / Sails : WEb App Framework for NodeJS

# NodeJS Installer -

- Node Runtime Environment (NRE)
- Node Native Module (path, http, fs etc)
- Node Package Manager (NPM) -

# Create-React-App (CLI Tool) to generate React Boilerplate project (Node Project)

# Atomic Design Principles

- Atom : Smallest unit; eg. button, an input field
- Molecule : comb of atoms; eg. searchbar -> one input field + one button
- Organism : comb of molecules; eg. navigation bar -> various links + SerachBar
- Template : comb of organism; eg. form -> input fileds, dropdowns, checkboxes etc
- Page : comb of templates; eg. a complete page

# Thinking in React way

- if the code is reusable, then create 'Component'

- npx create-react-app first-app
- cd first-app
- npm start

- npm i bootstrap

# Class Based Component -

- Stateful / Container / Smart
- Extends the React.Component class
- Implements the render()
- Life Cycle Methods
  > Mounting : constructor, render, componentDidMount
  > Updating : shouldComponentUpdate, render, componentDidUpdate
  > Unmounting : componentWillUnmount

# Functional Components -

- Concept of Hooks (react v16.8)
- useState()
- useEffect()
- useContext()

# Form Validation Libraries -

- formik
- yup
- react-hook-form
- unform
- react-form
- final-form react-final-form

# useEffect -> to run the side effect code

- making remote server call
- subscribing the variables
- timer API
- state change

# useEffect() Flavors -

- useEffect(cb) :
  > CB will fire at the initial component rendering and for all the subsequent re-rendering of component
- useEffect(cb, []) - "componentDidMount"
  > CB will fire at the initial component rendering ONLY.
  > Use case : Loading the initial data in the app
- useEffect(cb, [Dependencies]) - "componentDidUpdate"
  > CB will fire at the initial component rendering
  > CB will fire whenever the mentioned dependencies change.
- useEffect(cb => clean-up-Fn, [Deps])
  > CB will fire at the initial component rendering
  > CB will fire whenever the mentioned dependencies change.
  > Clean-up function will fire just before the CB function for subsequent re-rendering
  > Clean-up function will NOT fire at initial rendering
  > Clean-up will fire just before component is about to unload - "componentWillUnmount"

CB -> state change -> Clean-up -> CB

/data> json-server --watch db.json --port=9000

# useReducer : to maintain the complex state

- next state depends upon the previous state
- next state depends upon the other state variables

# useCallback : returns the same callback for every re-rendering

# useMemo : return the same referential value

eg. An Array / An Object / A Function

useCallback(cb) === useMemo(() => cb)

# Components

# Props

# State

# Hooks

> useState
> useEffect
> useContext
> useRef
> useCallback
> useMemo
> useReducer

- Routing : react-router-dom@6

  > npm i react-router-dom

  > BrowserRouter : enabled HTML5 History API
  > Routes
  > Route : path="/expenses", element : <Expenses />
  > NavLink / Link : similar to Anchor element <NavLink>
  > useParams() : access of URL Route Parameters
  > useLocation() : access of URL; fetch Querystrings
  > useNavigate() : programmatically navigating the user
  > Redirect : <Navigate>
  > PageNotFound : "\*"
  > Nested / Child Routing : /posts/:postId
  > Lazy Loading and Suspense (fallback) Component : React.lazy(() => import(""))

# Redux Pattern : Strict unidirectional data flow

- Action : An object that must contain 'type' property
- Reducers : A Function that accepts prevState and action as parameters, and return newState
- Store : Holds the state, as well as functions to mnage the stored state

# npm i react-redux redux

- <Provider store={store}> <App/> </Provider>
- connect(mapStateToProps, mapDispatchToProps) (ConnectedComponent)

> mapStateToProps : maps the state available in store with Props of the Component
> mapDispatchToProps : maps the dispatch() available with store to the props of the component

- Component Level State : useState / useReducer
- Component Branch Level State : JWT Token in Context API / Redux (not optimized, complexity in App )

<AuthContext.Provider value={}>
<LoginContext.Provider value ={}>
<LoggerContext.Provider value ={}>
<Expenses />
</LoggerContext.Provider>
</LoginContext.Provider>
</AuthContext.Provider>

- App-wide State : Redux

# A State slice should be managed by its own reducer

# Middleware -> used to run any side effect / async operation

- Custom Middleware : logger
- Thunk : allow us to return the function from action creator
  > npm i redux-thunk
- Saga

- ErrorBoundary : Class based Component to handle JavaScript Errors in Child Components
  > componentDidCatch
  > Fallback UI
- Deployment
  > npm i firebase-tools -g
  > firebase login
  > firebase init
  > firebase deploy
